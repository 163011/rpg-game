using UnityEngine;
using UnityEngine.UI;

public class BuyScript : MonoBehaviour
{
    public GameObject shopUI;

    public int[] amt;
    public int[] cost;
    public int[] iconNum;
    public int[] inventoryItems;
    public Text[] itemAmtText;
    public Text currencyText;

    private Text compare;
    public bool tavern = false;

    void Start()
    {
        UpdateGold();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void CloseShop()
    {
        shopUI.SetActive(false);
    }

    public void BuyButton()
    {
        for (int i = 0; i < itemAmtText.Length; i++)
        {
            if (itemAmtText[i] == compare)
            {

                if (amt[i] > 0)
                {
                    if (tavern)
                    {
                        UpdateTavernAmt();
                    }
                    else
                    {
                        UpdateWizardAmt();
                    }
                    if (InventoryItems.gold >= cost[i])
                    {
                        if (inventoryItems[i] == 0)
                        {
                            InventoryItems.newIcon = iconNum[i];
                            InventoryItems.iconUpdate = true;
                        }
                        InventoryItems.gold -= cost[i];
                        if (tavern)
                        {
                            SetTavernAmt(i);
                        }
                        else
                        {
                            SetWizardAmt(i);
                        }
                    }
                }

                break;
            }
        }
    }

    private void UpdateTavernAmt()
    {
        inventoryItems[0] = InventoryItems.bread;
        inventoryItems[1] = InventoryItems.cheese;
        inventoryItems[2] = InventoryItems.meat;
    }

    private void UpdateWizardAmt()
    {
        inventoryItems[0] = InventoryItems.redPotion;
        inventoryItems[1] = InventoryItems.purplePotion;
        inventoryItems[2] = InventoryItems.bluePotion;
        inventoryItems[3] = InventoryItems.greenPotion;
        inventoryItems[4] = InventoryItems.dragonEgg;
        inventoryItems[5] = InventoryItems.roots;
        inventoryItems[6] = InventoryItems.leafDew;
    }

    private void SetWizardAmt(int item)
    {
        switch (item)
        {
            case 0:
                InventoryItems.redPotion++;
                break;
            case 1:
                InventoryItems.purplePotion++;
                break;
            case 2:
                InventoryItems.bluePotion++;
                break;
            case 3:
                InventoryItems.greenPotion++;
                break;
            case 4:
                InventoryItems.dragonEgg++;
                break;
            case 5:
                InventoryItems.roots++;
                break;
            case 6:
                InventoryItems.leafDew++;
                break;
        }
        UpdateAll(item);
    }

    private void UpdateAll(int item)
    {
        amt[item]--;
        itemAmtText[item].text = amt[item].ToString();
        UpdateGold();
    }

    private void SetTavernAmt(int item)
    {
        switch (item)
        {
            case 0:
                InventoryItems.bread++;
                break;
            case 1:
                InventoryItems.cheese++;
                break;
            case 2:
                InventoryItems.meat++;
                break;
        }
        UpdateAll(item);
    }

    public void Bread()
    {
        compare = itemAmtText[0];
    }

    public void Cheese()
    {
        compare = itemAmtText[1];
    }

    public void Meat()
    {
        compare = itemAmtText[2];
    }

    public void RedPotion()
    {
        compare = itemAmtText[0];
    }

    public void PurplePotion()
    {
        compare = itemAmtText[1];
    }

    public void BluePotion()
    {
        compare = itemAmtText[2];
    }

    public void GreenPotion()
    {
        compare = itemAmtText[3];
    }

    public void DragonEgg()
    {
        compare = itemAmtText[4];
    }

    public void Roots()
    {
        compare = itemAmtText[5];
    }

    public void LeafDew()
    {
        compare = itemAmtText[6];
    }

    public void UpdateGold()
    {
        currencyText.text = InventoryItems.gold.ToString();
    }
}
