using Cinemachine;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{

    private NavMeshAgent nav;
    private Ray ray;
    private RaycastHit hit;
    private Animator anim;

    private float velocitySpeed;
    private float x;
    private float z;

    CinemachineTransposer ct;
    public CinemachineVirtualCamera playerCam;
    private Vector3 pos;
    private Vector3 currPos;


    public static bool canMove = true;
    public static bool isMoving = false;
    public LayerMask moveLayer;
    // Start is called before the first frame update

    public GameObject freeCam;
    public GameObject staticCam;
    private bool freeCamActive = true;

    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        ct = playerCam.GetCinemachineComponent<CinemachineTransposer>();
        currPos = ct.m_FollowOffset;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Cal velocity speed
        x = nav.velocity.x;
        z = nav.velocity.z;
        velocitySpeed = x + z;

        //Get mouse/cursor position
        pos = Input.mousePosition;
        ct.m_FollowOffset = currPos;

        if (Input.GetMouseButtonDown(1) && canMove) // 0 is left, 1 is right
        {
            ray = Camera.main.ScreenPointToRay(pos);
            if (Physics.Raycast(ray, out hit, 300, moveLayer))
            {
                nav.destination = hit.point;
            }
        }

        /*    //Player control camera
            if (Input.GetMouseButton(0))
            {
                if (pos.x != 0 || pos.y != 0)
                    currPos = pos / 200;
            }*/

        anim.SetBool("sprinting", velocitySpeed != 0);
        isMoving = velocitySpeed != 0;

        if (Input.GetKeyDown(KeyCode.C))
        {
            freeCam.SetActive(!freeCamActive);
            staticCam.SetActive(freeCamActive);
            freeCamActive = !freeCamActive;
        }

    }

}
