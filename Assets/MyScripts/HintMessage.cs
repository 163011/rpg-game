using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HintMessage : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    public GameObject hintBox;
    public Text message;
    private Vector3 screenPoint;
    public int objectType = 0;
    public GameObject theCanvas;
    public Sprite CursorBasic;
    public Sprite CursorHand;
    public Image CursorImage;
    private bool overIcon = false;
    public GameObject inventoryObject;
    public bool magic = false;
    public bool spells = false;
    public bool changePosition = false;

    public void OnPointerEnter(PointerEventData eventData)
    {
        overIcon = true;
        CursorImage.sprite = CursorHand;
        hintBox.SetActive(true);
        screenPoint.x = changePosition ? Input.mousePosition.x - 400 : Input.mousePosition.x + 400;
        screenPoint.y = Input.mousePosition.y;
        screenPoint.z = 1f;
        MessageDisplay();
        hintBox.transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        CursorImage.sprite = CursorBasic;
        hintBox.SetActive(false);
        overIcon = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && overIcon)
        {
            if (magic)
            {
                if (objectType != 0)
                {
                    inventoryObject.GetComponent<InventoryItems>().selected = objectType - 20;
                    inventoryObject.GetComponent<InventoryItems>().set = true;

                }
            }
            if (spells)
            {
                if (objectType != 0)
                {
                    inventoryObject.GetComponent<InventoryItems>().selected = objectType - 30;
                    inventoryObject.GetComponent<InventoryItems>().setTwo = true;

                }
            }
        }
    }

    void Start()
    {
        hintBox.SetActive(false);
    }

    private void MessageDisplay()
    {
        switch (objectType)
        {
            case 1:
                message.text = InventoryItems.redMushrooms.ToString() + " red mushrooms to be used in potions";
                break;
            case 2:
                message.text = InventoryItems.purpleMushrooms.ToString() + " purple mushrooms to be used in potions";
                break;
            case 3:
                message.text = InventoryItems.brownMushrooms.ToString() + " brown mushrooms to be used in potions";
                break;
            case 4:
                message.text = InventoryItems.blueFlowers.ToString() + " blue flowers to be used in potions";
                break;
            case 5:
                message.text = InventoryItems.redFlowers.ToString() + " red flowers to be used in potions";
                break;
            case 6:
                message.text = InventoryItems.roots.ToString() + " roots to be used in potions";
                break;
            case 7:
                message.text = InventoryItems.leafDew.ToString() + " leaf dew to be used in potions";
                break;
            case 8:
                message.text = "Key to open chests";
                break;
            case 9:
                message.text = InventoryItems.dragonEgg.ToString() + " dragon eggs to be used in potions";
                break;
            case 10:
                message.text = InventoryItems.bluePotion.ToString() + " blue potion to be used in potions";
                break;
            case 11:
                message.text = InventoryItems.purplePotion.ToString() + " purple potion to be used in potions";
                break;
            case 12:
                message.text = InventoryItems.greenPotion.ToString() + " green potion to be used in potions";
                break;
            case 13:
                message.text = InventoryItems.redPotion.ToString() + " red potion to be used in potions";
                break;
            case 14:
                message.text = InventoryItems.bread.ToString() + " bread used to replenish health";
                break;
            case 15:
                message.text = InventoryItems.cheese.ToString() + " cheese used to replenish health";
                break;
            case 16:
                message.text = InventoryItems.meat.ToString() + " meat used to replenish health";
                break;
            case 20:
                message.text = "explosive fire attack";
                break;
            case 21:
                message.text = "replenishes full health";
                break;
            case 22:
                message.text = "become invisible for as long as mana";
                break;
            case 23:
                message.text = "become invulnerable for as long as mana lasts";
                break;
            case 24:
                message.text = "double strength for as long as mana lasts";
                break;
            case 25:
                message.text = "swirl attack";
                break;
            case 30:
                message.text = "magic attack 1";
                break;
            case 31:
                message.text = "magic attack 2";
                break;
            case 32:
                message.text = "magic attack 3";
                break;
            case 33:
                message.text = "magic attack 4";
                break;
            case 34:
                message.text = "magic attack 5";
                break;
            case 35:
                message.text = "magic attack 6";
                break;
            case 0:
            default:
                message.text = "Empty";
                break;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        theCanvas.GetComponent<CreatePotion>().thisValue = objectType;
        theCanvas.GetComponent<CreatePotion>().UpdateValues();
    }
}
