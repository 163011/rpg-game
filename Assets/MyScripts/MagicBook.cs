using UnityEngine;
using UnityEngine.UI;

public class MagicBook : MonoBehaviour
{
    public Image magicIcon;
    public Text magicName;
    public Text description;
    public Sprite[] magicSprites;
    public string[] names;
    public string[] descriptions;
    public GameObject[] iconSets;
    public GameObject nextBtn;
    public GameObject backBtn;
    public GameObject theCanvas;

    private int currentIcon = 0;

    // Start is called before the first frame update
    void Start()
    {
        backBtn.SetActive(false);
        magicIcon.sprite = magicSprites[0];
        magicName.text = names[0];
        description.text = descriptions[0];
        iconSets[0].SetActive(true);
        for (int i = 1; i < iconSets.Length; i++)
        {
            iconSets[i].SetActive(false);
        }
    }

    public void Next()
    {
        if (currentIcon < magicSprites.Length - 1)
        {
            backBtn.SetActive(true);
            currentIcon++;
            magicIcon.sprite = magicSprites[currentIcon];
            magicName.text = names[currentIcon];
            description.text = descriptions[currentIcon];
            SwitchOff();
            iconSets[currentIcon].SetActive(true);
            theCanvas.GetComponent<CreatePotion>().itemID++;
            theCanvas.GetComponent<CreatePotion>().value = 0;
            theCanvas.GetComponent<CreatePotion>().thisValue = 0;
            if (currentIcon == magicSprites.Length - 1)
            {
                nextBtn.SetActive(false);
            }
        }
    }

    public void Back()
    {
        if (currentIcon > 0)
        {
            nextBtn.SetActive(true);
            currentIcon--;
            magicIcon.sprite = magicSprites[currentIcon];
            magicName.text = names[currentIcon];
            description.text = descriptions[currentIcon];
            SwitchOff();
            iconSets[currentIcon].SetActive(true);
            theCanvas.GetComponent<CreatePotion>().itemID--;
            theCanvas.GetComponent<CreatePotion>().value = 0;
            theCanvas.GetComponent<CreatePotion>().thisValue = 0;
            if (currentIcon == 0)
            {
                backBtn.SetActive(false);
            }
        }

    }

    private void SwitchOff()
    {
        for (int i = 0; i < iconSets.Length; i++)
        {
            iconSets[i].SetActive(false);
        }
    }
}
