using UnityEngine;
using UnityEngine.UI;

public class Cursors : MonoBehaviour
{
    public GameObject cursorObject;
    public Sprite cursorBasic;
    public Sprite cursorHand;
    public Image cursorImage;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
    }

    public void ChangeCursorHand()
    {
        cursorImage.sprite = cursorHand;
    }

    public void ChangeCursorBasic()
    {
        cursorImage.sprite = cursorBasic;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        cursorObject.transform.position = Input.mousePosition;
        if (Input.GetMouseButton(0))
        {
            cursorImage.sprite = cursorHand;
        }
        if (Input.GetMouseButtonUp(0))
        {
            cursorImage.sprite = cursorBasic;
        }
    }

}
